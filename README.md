# Intro to Programming

This repository is intended to help new students understand the basics of FRC
robot programming.

It is broken up into eleven sections:

- [Setting Up](0-setting-up)
- [Modify Robot Code](1-modify-robot-code)
- [Digital Sensors](2-digital-sensors)
- [Analog Sensors](3-analog-sensors)
- [Spin a Motor](4-spin-a-motor)
- [Encoders](5-encoders)
- [Pneumatics](6-pneumatics)
- [Controller Inputs](7-controller-inputs)
- [Command-Based Programming](8-command-based-programming)
- [Autonomous Operations](9-autonomous-operations)
- [Feedback Control](10-feedback-control)

Each of these sections aims to teach a particular concept, or set of concepts,
commonly used in FRC robot programming.

It is highly recommended that you complete these in order as each will build
off of the previous sections.

## Contributors

### 3786 Charger Robotics

[![3786_logo]](https://chargerrobotics.com)

|  |  |
| ---- | ---- |
| Code Repositories | [![gitlab_logo_small]](https://gitlab.com/chargerrobotics3786)|

If you would like to contribute, see [CONTRIBUTING.md](CONTRIBUTING.md)

<!-- Image References -->
<!-- Small Logo images should be 25px square,
       larger team logos should be 100px in height-->
<!-- markdownlint-disable MD053 -->
[gitlab_logo_small]: resources/gitlab-logo-small.png "GitLab"
[3786_logo]: resources/3786-logo.png "3786 Charger Robotics"
