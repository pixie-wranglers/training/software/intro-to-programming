# Getting Setup

This section will walk you through getting your [development environment](glossary#development-environment)
set up and deploying your first code to the robot.

## Development Environment Setup

To set up your development environment, follow the
 [latest set of instructions from WPILib](https://docs.wpilib.org/en/stable/docs/zero-to-robot/step-2/wpilib-setup.html)

- When prompted to select a choice, use the `Download for this computer only`
 option.

That's it! In the next step, we'll check to be sure everything is working.

## Creating an Example Project

Follow the instructions from WPILib's documentation on [Creating a New WPILib Project](https://docs.wpilib.org/en/stable/docs/software/vscode-overview/creating-robot-program.html#creating-a-new-wpilib-project).

That document lays out the 8 options when creating a project. When you do, use
 these values:

1. Example
2. Java
3. Tank Drive Xbox Controller
4. Find somewhere on your computer to store all of your robot projects
5. `TankDriveExample`
6. Check this box
7. `3786`
8. Leave this unchecked for now

Open the [Command Palette](glossary#command-palette) (`Ctrl + Shift + P`),
 and locate the `WPILib: Create a new project` command.

## Deploying the Example Project

Once you've finished creating the example project, let's deploy it and run it!

> **_Note:_** You may wish to seek out the assistance of a veteran team member
> or mentor for this part.

Power on the robot.

Connect to the robot's network.

Open the [Command Palette](glossary#command-palette) again, and find the
 `WPILib: Deploy` command.

On the Driver Station laptop, connect to the robot's network, and run the Driver
 Station program.

To know what all of the parts of the Driver Station do, see the
 [Anatomy of the Driver Station](https://docs.wpilib.org/en/stable/docs/software/driverstation/driver-station.html)

> :warning: Ensure that everyone is clear of the robot and are wearing safety glasses.

Click the Enable button.
