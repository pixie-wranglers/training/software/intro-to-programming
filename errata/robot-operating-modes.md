# WPILib Robot Operating Modes

An WPILib Robot object has 16 possible operating modes. These can also be considered
 life-cycle hooks for inserting logic into different stages of a robot's operation.
 These operating modes are as follows:

- `robotInit`
- `robotPeriodic`
- `autonomousInit`
- `autonomousPeriodic`
- `autonomousExit`
- `teleopInit`
- `teleopPeriodic`
- `teleopExit`
- `disabledInit`
- `disabledPeriodic`
- `disabledExit`
- `testInit`
- `testPeriodic`
- `testExit`
- `simulationInit`
- `simulationPeriodic`

You'll notice that there are actually 6 different modes, each with an `Init` and
 `Periodic` life-cycle hook, some even have an `Exit` hook. In general the `Init`
 method will be called at the entry into that mode and `Periodic` will be called
 at a regular interval while in the respective mode. The 4 `Exit` methods are called
 just before the execution leaves that mode and enters a new mode.

## The `robot` "mode"

This is not really a mode per se, as it is in effect for the entire duration of
 the robot being powered on.

### `robotInit`

This method gets called at the very beginning of the robot life-cycle. It should
 be used to construct and initialize things that need to persist for the whole
 time the robot is being run. Typically motors and controllers should be instantiated
 in this method.

### `robotPeriodic`

This method gets called periodically throughout the operation of the robot
 life-cycle. This should be used for logic that needs to be running the whole
 time that the robot is running.

## The `autonomous` mode

This mode is when the robot is running in the autonomous period of a match.

### `autonomousInit`

As discussed previously this method will get called when entering the autonomous
 mode. A typical use of this method is to select differing autonomous algorithms
 with a [`SendableChooser`](https://docs.wpilib.org/en/latest/docs/software/dashboards/smartdashboard/choosing-an-autonomous-program-from-smartdashboard.html#creating-sendablechooser-object).

### `autonomousPeriodic`

This method should include the code that you want to execute during the autonomous
 period.

### `autonomousExit`

Runs right before exiting autonomous mode. If running a command-based algorithm
 this can be used to cancel the command group(s).

## The `teleop` mode

This mode is when the robot is operating under full control of the driver and
 operator.

### `teleopInit`

Run right before entring teleop mode.

### `teleopPeriodic`

This is the method used for the bulk of running a robot handling user input and
 interacting with sensors and outputs.

### `teleopExit`

Runs at the end of teleop.

## The `disabled` mode

This mode is active when the robot is disabled. PWM output and CAN motor controllers
 are disabled regardless of [`runsWhenDisabled`](https://docs.wpilib.org/en/latest/docs/software/commandbased/commands.html#runswhendisabled).

### `disabledInit`

Runs when entering the disabled state.

### `disabledPeriodic`

Runs while the robot is disabled. Can abe used to control any non-PWM or CAN devices.

### `disabledExit`

Runs right before leaving the disabled state.

## The `test` mode

Another similar mode to autonomous and teleop that can be run outside of the field
 to help with troubleshooting.

### `testInit`

Runs when entering test mode.

### `testPeriodic`

This is where you'll put your code for running system tests.

### `testExit`

This is used to cleanup after test mode.

## The `simulation` mode

This is another special mode similar to `robot` that runs during the entirity of
 a robot's lifecycle while in simulation mode. All subsystems have simulation
 methods to define how they run while in simulation. This is for advanced usage.

### `simulationInit`

Runs at the beginning of a simulation run to initialize all the simulation only
 variables.

### `simulationPeriodic`

Run throughout the lifecycle for a simulation run. Can be used to capture outputs
 and general system feedback that is required to validate a simulation's performance.
