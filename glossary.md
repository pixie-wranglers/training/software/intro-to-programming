# Glossary

## CAN

CAN (Controller Area Network) is an industry standard communication protocol commonly
 used in automotive applications. This is one way for different systems to communicate.

See [CAN bus](https://en.wikipedia.org/wiki/CAN_bus), and Intro to Electronics.

## Command Palette

The Command Palette in Visual Studio Code is a feature that provides an interactive
 text-based interface to execute various commands, access settings, and perform
 actions within the code editor.

The Command Palette can be invoked with the keyboard shortcut `Ctrl + Shift + P`.

For all of the available WPILib commands, see the [relevant documentation](https://docs.wpilib.org/en/stable/docs/software/vscode-overview/wpilib-commands-vscode.html#wpilib-commands-in-visual-studio-code).

## Compiler

A compiler software that translates human-readable source code into machine code,
 making it executable by a computer. It plays a fundamental role in converting
 high-level programming languages (such as Java or C++) into instructions that
 a computer's central processing unit (CPU) can understand and execute.

## Debug

Debugging is the process of identifying and eliminating errors or bugs in software
 code. It involves locating, analyzing, and fixing issues that cause the program
 to behave incorrectly or crash, ensuring it runs smoothly and as intended.

## Development Environment

A development environment is a programmer's setup that they use to create, test,
 and [debug](#debug) software. It includes tools like text editors, [compilers](#compiler),
 [version control](#version-control), and [testing frameworks](#software-testing),
 essential for efficient software development.

See [IDE](#ide-integrated-development-environment)

## Driver Station

The Driver Station is a software tool developed by National Instruments. It
 serves as the control hub for FRC robots, enabling teams to operate and monitor
 their robots during competitions or at home. The Driver Station provides a user-friendly
 interface for sending commands to the robot, receiving real-time data and
 diagnostics, and ensuring safe and effective robot operation.

See [Anatomy of the Driver Station](https://docs.wpilib.org/en/stable/docs/software/driverstation/driver-station.html).

## IDE (Integrated Development Environment)

An IDE, or Integrated Development Environment, is a software application that
 offers a comprehensive set of tools for coding, debugging, and managing projects
 in one unified interface.It streamlines the software development process, enhancing
 productivity for programmers.

See [Development Environment](#development-environment)

## Instantiation

This is the act of creating a new object of a particular class. See Learning Java
 for more information on objects, classes, and instantiation.

## JavaDoc

This is a helpful comment typically left with each method and class that documents
 uses, inputs, and outputs. Sometimes algorithms are described as well. This is a
 well defined industry standard and helps VSCode display its help pop-ups.

## Motor Controller

An electronic device that takes some input signal(s) and converts that to a signal
 to drive a motor. Typically they will also collect and share data about the current
 state of the motor.

## Software Testing

Software Testing is the process of evaluating a software application to identify
 and rectify defects or issues. It ensures the software functions correctly,
 meets requirements, and delivers a high level of quality and reliability.
 Testing involves various methods and techniques, including unit testing,
 integration testing, and user acceptance testing.

## Version Control

Version Control is a system that tracks and manages changes to source code and
 other files, enabling collaboration, tracking revisions, and maintaining a
 history of edits. It ensures code integrity and simplifies team-based development.
