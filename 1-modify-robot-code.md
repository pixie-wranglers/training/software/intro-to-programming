# Instructions for Modifying Robot Code

This tutorial is designed to walk you through the process of modifying basic
 robot drive code to add or change features. By the end of this tutorial, you should
 have a basic understanding of:

- Robot Operating Modes
- Setup and Loop methods
- Defining, declaring, and interacting with objects
- Calling methods

## Setting Up

1. Open the project from the [previous exercise](../0-setting-up.md).
2. Open the `src/main/java/frc/robot/Robot.java` file.

## Reading the Robot.java File

### The `Robot` Class

The `Robot` class is at the heart of every (Java) FRC robot. It handles the
 life-cycle of the robot throughout a match. This robot is a `TimedRobot` so
 it extends the `TimedRobot` class, we will discuss the implications of this
 in future exercises.

```java
public class Robot extends TimedRobot {
  private DifferentialDrive m_myRobot;
  private Joystick m_leftStick;
  private Joystick m_rightStick;

  private final MotorController m_leftMotor = new PWMSparkMax(0);
  private final MotorController m_rightMotor = new PWMSparkMax(1);
  /*
  ...
  */
}
```

This example requires a few class members to be declared and [instantiated](glossary#instantiation).

- `m_myRobot` is a [`DifferentialDrive`](https://github.wpilib.org/allwpilib/docs/release/java/edu/wpi/first/wpilibj/drive/DifferentialDrive.html)
  object used for controlling the drive base.
- `m_leftStick` and `m_rightStick` are [`Joystick`](https://github.wpilib.org/allwpilib/docs/release/java/edu/wpi/first/wpilibj/Joystick.html)
  objects used to get inputs from the driver.
- `m_leftMotor` and `m_rightMotor` are [`MotorController`](https://github.wpilib.org/allwpilib/docs/release/java/edu/wpi/first/wpilibj/motorcontrol/MotorController.html)
  objects that are [instantiated](glossary#instantiation) as a [`PWMSparkMax`](https://github.wpilib.org/allwpilib/docs/release/java/edu/wpi/first/wpilibj/motorcontrol/PWMSparkMax.html)
  with the respective [CAN](glossary#can) ID for each.

See Learning Java for a description of the syntax used.

### Operating Modes

In the provided example code, the `robotInit` and `teleopPeriodic`
 [operating modes](errata/robot-operating-modes.md) are used.
 At a high level, `robotInit` contains the code to set up the [motor controllers](glossary#motor-controller)
 and joysticks. `teleopPeriodic`reads the joystick values and sends them to the
 `DifferentialDrive`'s `tankDrive` method.

### Understanding the `robotInit` method

`robotInit` does several important setup steps for the robot.

```java
  public void robotInit() {
    m_rightMotor.setInverted(true);

    m_myRobot = new DifferentialDrive(m_leftMotor, m_rightMotor);
    m_leftStick = new Joystick(0);
    m_rightStick = new Joystick(1);
  }
```

The first method call is to invert the direction of `m_rightmotor`. Since this is
 a tank drive one of the motors needs to have its rotation inverted. This is
 done with the `setInverted` method of the `MotorController` class.

Next we initialize the remaining class members that we declared earlier. `m_myRobot`
 is constructed with a new `DifferentialDrive` using `m_leftMotor` and `m_rightMotor`.
 `m_leftStick` and `m_rightStick` are constructed as new `Joystick`s passing in the
 correct id for the USB joysticks connected to the [Driver Station](glossary#driver-station).

### Dissecting `teleopPeriodic` Method

The contents of the `teleopPeriodic` method can be split into a few different steps
 as described in the subsections. Even though there's only one line of code in this
 method there's a lot to unpack and explain.

```java
public void teleopPeriodic() {
  m_myRobot.tankDrive(-m_leftStick.getY(), -m_rightStick.getY());
}
```

#### Reading a Joystick Value

The method call, `m_leftStick.getY()`
 asks the `m_leftStick` object to get the value of its Y axis, which is
 the forward and back movement of the joystick. The same thing is done for
 `m_rightStick` to get the value of its Y axis. This is the basis of a simple
 [tank drive](https://docs.wpilib.org/en/stable/docs/software/hardware-apis/motors/wpi-drive-classes.html#drive-modes).

#### Inverting the Joysick Input Value

On most controllers, the joystick values come out "backwards" from where you
 generally want them for robots (stick forward corresponds to `-1.0` and stick back
 corresponds to `1.0`). To correct this before writing the value, we negate
 the value returned by the `getY` methods.

#### Sending the Value to the `tankDrive`

The negated values are then passed directly to a method call of the `DifferentialDrive`
 object's `tankDrive` method. This class will manage the control of the motors
 for you. This method call has a return type of `void` so we can't assign its output
 to a variable since there isn't any output. For more information on classes see
 the Learning Java project.

## Modifying the Code

In this section we'll be switching the 2 motors and observing the effect. The left
 motor will become the right and the right will become the left. To do this all we
 need to do is change the channel that each motor is constructed with.

Lines 22 and 23 become:

```java
private final MotorController m_leftMotor = new PWMSparkMax(1);
private final MotorController m_rightMotor = new PWMSparkMax(0);
```

Build and deploy the new code and observe that the robot is now oriented such that
 a command from the driver to go forward will actually make the robot go backwards.
 This is one way to cause the robot to change which way it thinks is forward.

## Exercise Left to the Reader

This next section will give you some simple assignments for code modification.
 __The information to complete these sections is not fully contained in this document__.
 These will require you to reach out to some external resources. The first place
 to find information is Google. If you can't find information there, or you have
 trouble understanding it, the next place to consult is your peers, and then mentors.

### Adding a 'Reverse' Button

For the exercise you'll be implementing a `reverse` button that makes the robot
 drive backwards instead of forwards similar to what we did in the above example,
 except this time the change needs to be revertable without deploying new code
 to the robot everytime. This should be done using one of the auxiliary buttons
 on one of the joysticks.

Hints:

1. Familiarize yourself with the following Java constructs:
    - `if-else` statements
2. Create a new `boolean` variable, and read the value of a Joystick button to it.
    - This will require browsing the [_Javadocs_](glossary#javadoc) for FRC.
      The WPILib [Javadocs](glossary#javadoc) are found [here](https://first.wpi.edu/wpilib/allwpilib/docs/release/java/index.html).
3. Think about how the tank drive gets its commands from the user.

Once you've completed the exercise, review you're code with a mentor.
