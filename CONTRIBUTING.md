# Contributing

Thanks for being interested in contributing!

For information on what this project needs, see the issues list.

## Linting Practices

This project is linted using [markdownlint](https://github.com/DavidAnson/markdownlint).
This is enforced via the CI pipeline.

The [markdownlint](https://github.com/DavidAnson/markdownlint) project has
details on how to install it locally should you wish to.

## Author Credits

If this is your first time contributing and you are affiliated with an
FRC team, please add your team's logo, and relevant links,
to the [Contributors section of the README](README.md#contributors).
